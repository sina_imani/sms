# IAWT

import threading
from kavenegar import *
from datetime import datetime

API_KEY = '424D73527866673846614F37456C442F31766775334663546A32666573625832464A7748496569367564673D'

sender_num = 1000596446

status_forms = ['succesfully sent', 'connected but not sent', 'could not connect']


def pure_send(receptor, message, sender=sender_num):
    api = KavenegarAPI(API_KEY)
    prms = {
        'sender': sender,
        'receptor': receptor,
        'message': message
    }
    try:
        file = open('sms_log.txt', 'a')
    except FileNotFoundError:
        file = open('sms_log.txt', 'w')

    try:
        api.sms_send(prms)
        result = '0' + sender + '>' + receptor + ':' + message
    except APIException:
        result = '1'
    except HTTPException:
        result = '2'
    file.write(datetime.now().ctime() + result + '/n')
    file.close()


def validation(number):
    return True


def send_sms(receptor, message, sender=sender_num):  # Returns True if a sending process has been started, Flase otherwise
    if not validation(receptor):
        return False
    send_thread = threading.Thread(target=pure_send, args=(receptor, message, sender))
    send_thread.start()
    return True

